
import Slider from "./components/Slider"
import ThemeSwitcher from "./components/ThemeSwitcher"

export default function App() {
  return (
    <div className=" flex justify-center items-center w-screen h-screen shadow-lg dark:bg-gray-900 border-gray-400">
      <div className="flex w-1/3 border-solid justify-between">
        <ThemeSwitcher />
        <Slider />
      </div>
    </div>
  );
}
