import React, { useState, useEffect } from 'react'



export default function Slider () {

   const initialValue = 50.75;
   const [value, setValue] = useState(initialValue)
   const [mouseState, setMouseState] = useState('');

   const handleSliderChange = (event) => {
      setValue(event.target.value)
   };

   useEffect(() => {
      setValue(value)
   }, [value])

   useEffect(() => {
      setMouseState(mouseState)
   }, [mouseState])

   const handleBlur = () => {
      if(value < 0) {
         setValue(0.);
      } else if( value > 100) {
         setValue(100);
      }
   };

   return (
     <>
       <div className=" relative w-96 h-24 border rounded-2xl border-gray-200 shadow-lg flex justify-between items-center p-8 dark:border-gray-400 dark:bg-gray-800">
         <div className="w-full">
            {/* <div className="absolute bottom-6">
               <svg width="200" height="10" viewBox="0 0 142 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <rect y="1.99999" width="140" height="2" rx="1" fill={blue}/>
                  <rect x="1" y="1.99999" width="90" height="2" rx="1" fill={blue}/>
                  <circle cx="2.52803" cy="2.55465" r="1.89607" fill={blue} stroke={blue} stroke-width="1.26405"/>
                  <circle cx="2.52803" cy="2.55465" r="1.89607" fill="white" stroke={blue} stroke-width="1.26405"/>
                  <circle cx="2.99994" cy="3.00001" r="2.36798" fill={blue} stroke={blue} stroke-width="1.26405"/>
                  <circle cx="37" cy="2.99999" r="3" fill={blue}/>
                  <circle cx="88" cy="2.99999" r="2.25" fill="blue" stroke={blue} stroke-width="1.5"/>
                  <circle cx="139" cy="2.99999" r="3" fill={blue}/>
                  <circle cx="105" cy="2.99999" r="3" fill={blue}/>
                  <circle cx="71" cy="2.99999" r="3" fill={blue}/>
               </svg>
            </div> */}
           <input className="w-11/12"
            //  id="myinput"
             type="range"
             min="0"
             max="100"
             step="0.01"
             value={value}
             onChange={handleSliderChange}
             onMouseDown={() => setMouseState("down")}
             onMouseUp={() => setMouseState("up")}
           />
         </div>
         <span className="absolute right-10  dark:text-white">%</span>
         <input
           className=" w-20 border rounded-lg border-gray-200 shadow-lg flex justify-center items-center pr-4 pl-3 py-2 dark:bg-transparent dark:text-white"
           type='number'
           step="0.01"
           value={value}
           onChange={handleSliderChange}
           onBlur={handleBlur}
         >
         </input>
       </div>
     </>
   );
}